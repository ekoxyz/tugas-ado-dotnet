﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace tugas_ado_dotNet
{
    public partial class Form1 : Form
    {
        public MySqlConnection sqlConnection = new MySqlConnection(
            "SERVER = localhost; UID = root; PWD = ; DATABASE = adotnetdb;"
            );
        public MySqlDataAdapter adapter = new MySqlDataAdapter();
        public MySqlCommand command = new MySqlCommand();
        public DataSet mDataSet = new DataSet();
        public String ID="";

       

        public Form1()
        {
            InitializeComponent();
            getAll();
             
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            mDataSet = new DataSet();
            String query= "INSERT INTO `barang` (`sn`, `nama_barang`, `stock`, `harga`)" +
                "VALUES ('"+txtSn.Text+ "', '"+txtNama.Text+"', '"+txtStock.Text+"', '"+txtHarga.Text+"')";
            adapter = new MySqlDataAdapter(query, sqlConnection);
            adapter.Fill(mDataSet,"barang");
            MessageBox.Show("Added!");
            txtSn.Clear();
            txtNama.Clear();
            txtStock.Clear();
            txtHarga.Clear();
            getAll();
        }

        private void getAll()
        {
            mDataSet = new DataSet();
            String query = "SELECT * FROM `barang`";
            adapter = new MySqlDataAdapter(query, sqlConnection);
            adapter.Fill(mDataSet, "barang");

            viewDataTable.DataSource = mDataSet;
            viewDataTable.DataMember = "barang";
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            int i = viewDataTable.CurrentRow.Index;

            ID = viewDataTable[0, i].Value.ToString();
            txtSn.Text = viewDataTable[1,i].Value.ToString();
            txtNama.Text = viewDataTable[2, i].Value.ToString();
            txtStock.Text = viewDataTable[3, i].Value.ToString();
            txtHarga.Text = viewDataTable[4, i].Value.ToString();

        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            mDataSet = new DataSet();
            String query = "UPDATE `barang` SET `sn` = '"+txtSn.Text+"', `nama_barang` = '"
                +txtNama.Text+"', `stock` = '"+txtStock.Text+"', `harga` = '"
                +txtHarga.Text+"' WHERE `barang`.`id` ="+ID;

            adapter = new MySqlDataAdapter(query, sqlConnection);
            adapter.Fill(mDataSet, "barang");
            MessageBox.Show("Updated!");
            txtSn.Clear();
            txtNama.Clear();
            txtStock.Clear();
            txtHarga.Clear();
            getAll();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            int i = viewDataTable.CurrentRow.Index;
            ID = viewDataTable[0, i].Value.ToString();
            mDataSet = new DataSet();
            String query = "DELETE FROM `barang` WHERE `barang`.`id` ="+ID;
            adapter = new MySqlDataAdapter(query, sqlConnection);
            adapter.Fill(mDataSet, "barang");
            MessageBox.Show("Deleted!");
            getAll();
        }
    }
}
